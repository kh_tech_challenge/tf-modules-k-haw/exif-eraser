resource "aws_iam_role" "lambda_execution_role" {
  name = "${local.prefix}-lambda-execution-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  inline_policy {
    name   = "${local.prefix}-lambda-policy"
    policy = data.aws_iam_policy_document.lambda_policy.json
  }
}

data "aws_iam_policy_document" "lambda_policy" {
  statement {
    sid       = "sqs"
    actions   = ["sqs:DeleteMessage", "sqs:ReceiveMessage", "sqs:GetQueueAttributes"]
    resources = [aws_sqs_queue.image_processing_queue.arn]
  }
  statement {
    sid     = "cloudwatch"
    actions = ["logs:CreateLogStream", "logs:PutLogEvents"]
    resources = [
      aws_cloudwatch_log_group.exif_remover.arn,
      "${aws_cloudwatch_log_group.exif_remover.arn}:log-stream:*"
    ]
  }
  statement {
    sid       = "imageUploadBucketGetObject"
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.image_upload_bucket.arn}/*"]
  }
  statement {
    sid       = "processedImageBucketPutObject"
    actions   = ["s3:PutObject"]
    resources = ["${aws_s3_bucket.processed_image_bucket.arn}/*"]
  }
}