resource "aws_sqs_queue" "image_processing_queue" {
  name = "${local.prefix}-image-processing-queue"
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.image_processing_dead_letter_queue.arn
    maxReceiveCount     = local.sqs_event_max_attempts
  })

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "s3.amazonaws.com"  
      },
      "Action": "sqs:SendMessage",
      "Resource": "arn:aws:sqs:*:*:${local.prefix}-image-processing-queue",
      "Condition": {
        "ArnEquals": { "aws:SourceArn": "${aws_s3_bucket.image_upload_bucket.arn}" }
      }
    }
  ]
}
POLICY

  tags = local.tags
}

resource "aws_sqs_queue" "image_processing_dead_letter_queue" {
  name = "${local.prefix}-image-processing-dead-letter-queue"
  tags = local.tags
}