module "default_example" {
  source = "../.."

  ### GENERAL ###
  name = "default-example"
  tags = {
    test = "true",
  }

  ### S3 BUCKETS ###
  image_upload_bucket_expiration_after_days    = 1
  image_upload_bucket_versioning_enabled       = false
  processed_image_bucket_expiration_after_days = 1
  processed_image_bucket_versioning_enabled    = false
  artefact_bucket_versioning_enabled           = false

  ### SQS QUEUE ###
  sqs_event_max_attempts = 2

  ### CLOUDWATCH ###
  cloudwatch_log_retention_days = 1
}