output "upload_bucket_arn" {
  description = "ARN of upload bucket"
  value       = aws_s3_bucket.image_upload_bucket.arn
}

output "processed_bucket_arn" {
  description = "ARN of processed bucket"
  value       = aws_s3_bucket.processed_image_bucket.arn
}