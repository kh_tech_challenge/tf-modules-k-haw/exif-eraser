#!/bin/bash

# Quit if MODULE_PATH not set
if [ -z ${MODULE_PATH} ] ; then
    echo "Environment variable \$MODULE_PATH not set, quitting..."
    exit 1
fi

# create tmp_build directory in case it does not exist
mkdir ${MODULE_PATH}/tmp_build

# remove zip artefact in case of previous exection
rm -f ${MODULE_PATH}/tmp_build/exif_remover.zip

# create zip archive containing exif_remover.py
zip -j ${MODULE_PATH}/tmp_build/exif_remover.zip ${MODULE_PATH}/src/exif_remover.py