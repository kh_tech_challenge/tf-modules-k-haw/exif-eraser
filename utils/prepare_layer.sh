#!/bin/bash

# Quit if MODULE_PATH not set
if [ -z ${MODULE_PATH} ] ; then
    echo "Environment variable \$MODULE_PATH not set, quitting..."
    exit 1
fi

# remove zip artefact in case of previous exection
rm -f ${MODULE_PATH}/tmp_build/exif_remover_layer.zip

# execute pip3 install in a python container to ensure any C binaries are compiled for Linux
docker run -v ${MODULE_PATH}:/mnt python:3.9 pip3 install -r /mnt/src/requirements.txt --target ./mnt/tmp_build/layer/python/

# create a zip archive containing the required libraries
cd ${MODULE_PATH}/tmp_build/layer
zip -r ${MODULE_PATH}/tmp_build/exif_remover_layer.zip ./*