resource "aws_cloudwatch_log_group" "exif_remover" {
  name              = "/aws/lambda/${local.prefix}-exif-remover"
  retention_in_days = local.cloudwatch_log_retention_days
}