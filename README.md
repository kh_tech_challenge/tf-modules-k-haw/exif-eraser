# Exif Remover

## Description

The aim of this project is to provide the means of stripping exif data from JPEG images, using s3 object storage as both the input and output of the images to be processed.

This is achieved by using s3 event notifications & SQS in conjuctunction with lambda, the below diagram gives a high level overview of the flow:

![flow diagram](images/flow.png)

## Requirements
* Terraform 1.0.0 or greater
* Docker
* zip
* MacOS (tested) / Linux (not tested, but should work)

## Usage

This project is intended to be consumed as a Terraform module, refer to the Terraform documentation for sourcing github modules [here](https://www.terraform.io/docs/language/modules/sources.html#github).

If you are wanting to test or experiment with this module, you can check out the examples directory.

## Inputs

| Input | Description | Type | Default Value |
|---|---|---|---|
| name | Name of the deployment, used as a prefix in resource names | string | none |
| tags | Optional tags to create for each resource | map(string) | {} |
| image_upload_bucket_expiration_after_days | Controls expiration of objects within the image upload s3 bucket, if value is 0 objects will never expire | number | 0 |
| image_upload_bucket_versioning_enabled | Controls versioning of objects within the image upload s3 bucket | bool | false |
| processed_image_bucket_expiration_after_days | Controls expiration of image objects within the processed image s3 bucket, if value is 0 objects will never expire | number | 0 |
| processed_image_bucket_versioning_enabled | Controls versioning of image objects within the image upload s3 bucket | bool | false |
| artefact_bucket_versioning_enabled | Controls versioning of source code and lambda layer archive objects within the artefact s3 bucket | bool | true |
| sqs_event_max_attempts | Number of attempts to process the SQS event succesfully before moving to dead letter queue | number | 2 |
| cloudwatch_log_retention_days | Number of days of cloudwatch logs to retain | number | 14 |

## Outputs

| Output | Description |
|---|---|
| upload_bucket_arn | ARN of upload bucket |
| processed_bucket_arn | ARN of processed bucket |

## Roadmap
- [ ] KMS support for s3 bucket encryption
- [ ] s3 bucket cross region replication