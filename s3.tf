resource "aws_s3_bucket" "image_upload_bucket" {
  bucket = "${local.prefix}-image-upload-bucket"
  acl    = "private"

  lifecycle_rule {
    id      = "image_upload_retention"
    enabled = local.image_upload_bucket_expiration_enabled
    expiration {
      days = local.image_upload_bucket_expiration_after_days
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled = local.image_upload_bucket_versioning_enabled
  }

  tags = local.tags
}

resource "aws_s3_bucket" "processed_image_bucket" {
  bucket = "${local.prefix}-processed-image-bucket"
  acl    = "private"

  lifecycle_rule {
    id      = "processed_image_retention"
    enabled = local.processed_image_bucket_expiration_enabled
    expiration {
      days = local.processed_image_bucket_expiration_after_days
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled = local.processed_image_bucket_versioning_enabled
  }

  tags = local.tags
}

resource "aws_s3_bucket" "artefact_bucket" {
  bucket = "${local.prefix}-artefact-bucket"
  acl    = "private"
  tags   = local.tags

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled = local.artefact_bucket_versioning_enabled
  }
}

resource "aws_s3_bucket_object" "exif_remover_source_archive" {
  depends_on = [null_resource.prepare_lambda]
  bucket     = aws_s3_bucket.artefact_bucket.id
  key        = "exif_remover.zip"
  source     = "${path.module}/tmp_build/exif_remover.zip"

  source_hash = filemd5("${path.module}/src/exif_remover.py")
}

resource "aws_s3_bucket_object" "exif_remover_layer_archive" {
  depends_on = [null_resource.prepare_layer]
  bucket     = aws_s3_bucket.artefact_bucket.id
  key        = "exif_remover_layer.zip"
  source     = "${path.module}/tmp_build/exif_remover_layer.zip"

  source_hash = filemd5("${path.module}/src/requirements.txt")
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.image_upload_bucket.id

  queue {
    queue_arn = aws_sqs_queue.image_processing_queue.arn
    events    = ["s3:ObjectCreated:*"]
  }
}
