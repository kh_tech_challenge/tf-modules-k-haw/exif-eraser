locals {
  ### GENERAL ###
  prefix = var.name
  tags   = var.tags

  ### S3 BUCKETS ###
  image_upload_bucket_expiration_enabled       = var.image_upload_bucket_expiration_after_days > 0 ? true : false
  image_upload_bucket_expiration_after_days    = var.image_upload_bucket_expiration_after_days
  image_upload_bucket_versioning_enabled       = var.image_upload_bucket_versioning_enabled
  processed_image_bucket_expiration_enabled    = var.processed_image_bucket_expiration_after_days > 0 ? true : false
  processed_image_bucket_expiration_after_days = var.processed_image_bucket_expiration_after_days
  processed_image_bucket_versioning_enabled    = var.processed_image_bucket_versioning_enabled
  artefact_bucket_versioning_enabled           = var.artefact_bucket_versioning_enabled

  ### SQS QUEUE ###
  sqs_event_max_attempts = var.sqs_event_max_attempts

  ### CLOUDWATCH ###
  cloudwatch_log_retention_days = var.cloudwatch_log_retention_days
}