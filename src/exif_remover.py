#!/usr/bin/env python3

import json
import os
from PIL import Image
import boto3

s3 = boto3.resource('s3')

class Jpg_Image:
    def __init__(self, source_bucket, key, destination_bucket):
        self.source_bucket = source_bucket
        self.key = key
        self.destination_bucket = destination_bucket
        self.download_location = None
    
    def get(self):
        # set variable download_location to the path we want to download the image to
        # replace '/' with '_' to avoid FileNotFoundError exceptions
        self.download_location = '/tmp/' + self.key.replace('/', '_')

        # print notification to console and try downloading the s3 object
        print("Downloading s3 object s3://{0}/{1}...".format(self.source_bucket,self.key))
        try:
            s3.Bucket(self.source_bucket).download_file(self.key, self.download_location)
        except:
            raise Exception("Failed to download object from s3")

    def remove_exif(self):
        # try to process the image
        print('Removing exif data from image...')
        try:
            image = Image.open(self.download_location)
            data = list(image.getdata())
            image_without_exif = Image.new(image.mode, image.size)
            image_without_exif.putdata(data)
            image_without_exif.save(self.download_location)
        except:
            raise Exception("Failed to process image")
        print('Succesfully removed exif data')
        return True


    def put(self):
        # print notification to console and try uploading the s3 object
        print("Uploading s3 object to s3://{0}/{1}...".format(self.destination_bucket,self.key))
        try:
            s3.Bucket(self.destination_bucket).upload_file(self.download_location, self.key)
        except:
            raise Exception("Failed to upload object to s3")

def process_sqs_event(event):
    # initialise empty list to store s3 info
    image_locations = []

    # iterate through records in event, load json into python object
    for record in event['Records']:
        payload = record["body"]
        try:
            message = json.loads(payload)

            # iterate through 'Records' list and append s3 location dict to list
            for event in message['Records']:
                image_locations.append(
                    {
                        "bucket_name":event['s3']['bucket']['name'],
                        "key": event['s3']['object']['key'],
                    })
        except:
            try:
                # return empty list if we receive an s3:TestEvent, else raise an Exception
                if message['Event'] == "s3:TestEvent":
                    print('Received a test event')
                    return []
            except:
                raise Exception("Failed to process event message, is the message a valid s3 event?: " + str(payload))
        
    return image_locations

def lambda_handler(event, context):
    destination_s3_bucket = os.environ['DESTINATION_S3_BUCKET']
    image_locations = process_sqs_event(event)
    for image_location in image_locations:
        image = Jpg_Image(image_location['bucket_name'], image_location['key'], destination_s3_bucket)
        image.get()
        image.remove_exif()
        image.put()