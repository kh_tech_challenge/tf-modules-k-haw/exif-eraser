import unittest
from exif_remover import Jpg_Image, process_sqs_event

class TestRemoveExif(unittest.TestCase):
    def test_when_removing_exif_data_from_invalid_test_image_file_should_raise_Exception(self):
        test = Jpg_Image('does-not-matter', 'does-not-matter', 'does-not-matter')
        test.download_location = './test-data/invalid-file.jpg'
        self.assertRaises(Exception, test.remove_exif)

    def test_when_removing_exif_data_from_valid_test_image_file_should_return_true(self):
        test = Jpg_Image('does-not-matter', 'does-not-matter', 'does-not-matter')
        test.download_location = './test-data/image.jpg'
        actual = test.remove_exif()
        expected = True
        self.assertEqual(actual, expected)

class TestProcessSqsEvent(unittest.TestCase):
    def test_when_process_sqs_event_does_not_contain_s3_event_should_raise_Exception(self):
        event = {'Records': [{'messageId': '', 'receiptHandle': '', 'body': '{"test": "not-an-s3-event"}', 'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '', 'SenderId': '', 'ApproximateFirstReceiveTimestamp': ''}, 'messageAttributes': {}, 'md5OfBody': '', 'eventSource': 'aws:sqs', 'eventSourceARN': '', 'awsRegion': ''}]}
        self.assertRaises(Exception, process_sqs_event, event)
    
    def test_process_sqs_event_when_contains_multiple_s3_events_should_return_list_of_multiple_dicts(self):
        event = {'Records': [{'messageId': '', 'receiptHandle': '', 'body': '{"Records":[{"eventVersion":"2.1","eventSource":"aws:s3","awsRegion":"does-not-matter","eventTime":"0000-00-00T00:00:00.0000","eventName":"ObjectCreated:Put","userIdentity":{"principalId":""},"requestParameters":{"sourceIPAddress":""},"responseElements":{"x-amz-request-id":"","x-amz-id-2":""},"s3":{"s3SchemaVersion":"1.0","configurationId":"does-not-matter","bucket":{"name":"test-bucket-1","ownerIdentity":{"principalId":""},"arn":""},"object":{"key":"dir/file1.jpg","size":"0000","eTag":"","sequencer":""}}}, {"eventVersion":"2.1","eventSource":"aws:s3","awsRegion":"does-not-matter","eventTime":"0000-00-00T00:00:00.0000","eventName":"ObjectCreated:Put","userIdentity":{"principalId":""},"requestParameters":{"sourceIPAddress":""},"responseElements":{"x-amz-request-id":"","x-amz-id-2":""},"s3":{"s3SchemaVersion":"1.0","configurationId":"does-not-matter","bucket":{"name":"test-bucket-2","ownerIdentity":{"principalId":""},"arn":""},"object":{"key":"dir/file2.jpg","size":"0000","eTag":"","sequencer":""}}}]}', 'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '', 'SenderId': '', 'ApproximateFirstReceiveTimestamp': ''}, 'messageAttributes': {}, 'md5OfBody': '', 'eventSource': 'aws:sqs', 'eventSourceARN': '', 'awsRegion': ''}]}
        actual = process_sqs_event(event)
        expected = [
            {'bucket_name': 'test-bucket-1', 'key': 'dir/file1.jpg'}, 
            {'bucket_name': 'test-bucket-2', 'key': 'dir/file2.jpg'}
            ]
        self.assertEqual(actual, expected)

    def test_process_sqs_event_when_contains_s3_event_should_return_list_of_single_dict(self):
        event = {'Records': [{'messageId': '', 'receiptHandle': '', 'body': '{"Records":[{"eventVersion":"2.1","eventSource":"aws:s3","awsRegion":"does-not-matter","eventTime":"0000-00-00T00:00:00.0000","eventName":"ObjectCreated:Put","userIdentity":{"principalId":""},"requestParameters":{"sourceIPAddress":""},"responseElements":{"x-amz-request-id":"","x-amz-id-2":""},"s3":{"s3SchemaVersion":"1.0","configurationId":"does-not-matter","bucket":{"name":"test-bucket","ownerIdentity":{"principalId":""},"arn":""},"object":{"key":"dir/file.jpg","size":"0000","eTag":"","sequencer":""}}}]}', 'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '', 'SenderId': '', 'ApproximateFirstReceiveTimestamp': ''}, 'messageAttributes': {}, 'md5OfBody': '', 'eventSource': 'aws:sqs', 'eventSourceARN': '', 'awsRegion': ''}]}
        actual = process_sqs_event(event)
        expected = [{'bucket_name': 'test-bucket', 'key': 'dir/file.jpg'}]
        self.assertEqual(actual, expected)
