variable "name" {
  description = "Name of the deployment, used as a prefix in resource names"
  type        = string
}

variable "tags" {
  description = "Optional tags to create for each resource"
  type        = map(string)
  default     = {}
}

variable "image_upload_bucket_expiration_after_days" {
  description = "Controls expiration of objects within the image upload s3 bucket, if value is 0 objects will never expire"
  type        = number
  default     = 0
}

variable "image_upload_bucket_versioning_enabled" {
  description = "Controls versioning of objects within the image upload s3 bucket"
  type        = bool
  default     = false
}

variable "processed_image_bucket_expiration_after_days" {
  description = "Controls expiration of image objects within the processed image s3 bucket, if value is 0 objects will never expire"
  type        = number
  default     = 0
}

variable "processed_image_bucket_versioning_enabled" {
  description = "Controls versioning of image objects within the image upload s3 bucket"
  type        = bool
  default     = false
}

variable "artefact_bucket_versioning_enabled" {
  description = "Controls versioning of source code and lambda layer archive objects within the artefact s3 bucket"
  type        = bool
  default     = true
}

variable "sqs_event_max_attempts" {
  description = "Number of attempts to process the SQS event succesfully before moving to dead letter queue"
  type        = number
  default     = 2
}

variable "cloudwatch_log_retention_days" {
  description = "Number of days of cloudwatch logs to retain"
  type        = number
  default     = 14
}