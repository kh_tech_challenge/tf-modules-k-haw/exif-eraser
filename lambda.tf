resource "aws_lambda_function" "exif_remover" {
  s3_bucket     = aws_s3_bucket.artefact_bucket.id
  s3_key        = aws_s3_bucket_object.exif_remover_source_archive.id
  function_name = "${local.prefix}-exif-remover"
  role          = aws_iam_role.lambda_execution_role.arn
  handler       = "exif_remover.lambda_handler"
  layers        = [aws_lambda_layer_version.exif_remover_deps.arn]

  source_code_hash = filebase64sha256("${path.module}/src/exif_remover.py")

  runtime = "python3.9"

  depends_on = [
    aws_s3_bucket_object.exif_remover_source_archive,
    aws_iam_role.lambda_execution_role,
    aws_cloudwatch_log_group.exif_remover,
  ]

  environment {
    variables = {
      DESTINATION_S3_BUCKET = aws_s3_bucket.processed_image_bucket.id
    }
  }
}

resource "aws_lambda_layer_version" "exif_remover_deps" {
  depends_on = [aws_s3_bucket_object.exif_remover_layer_archive]
  s3_bucket  = aws_s3_bucket.artefact_bucket.id
  s3_key     = aws_s3_bucket_object.exif_remover_layer_archive.id
  layer_name = "${local.prefix}-lambda-layer"

  compatible_runtimes = ["python3.9"]
}

resource "aws_lambda_event_source_mapping" "sqs_event" {
  event_source_arn = aws_sqs_queue.image_processing_queue.arn
  function_name    = aws_lambda_function.exif_remover.arn
}

resource "null_resource" "prepare_layer" {
  triggers = {
    requirements_checksum = filebase64sha256("${path.module}/src/requirements.txt")
  }

  provisioner "local-exec" {
    command = "MODULE_PATH=${abspath(path.module)} ${path.module}/utils/prepare_layer.sh"
  }
}

resource "null_resource" "prepare_lambda" {
  triggers = {
    exif_remove_source_code_checksum = filebase64sha256("${path.module}/src/exif_remover.py")
  }
  provisioner "local-exec" {
    command = "MODULE_PATH=${abspath(path.module)} ${path.module}/utils/prepare_lambda.sh"
  }
}
